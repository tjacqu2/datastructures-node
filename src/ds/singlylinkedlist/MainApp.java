package ds.singlylinkedlist;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CircularLinkedList sll = new CircularLinkedList();
		sll.insertFirst(100);
		sll.insertFirst(200);
		sll.insertFirst(300);
		sll.insertLast(500);
		sll.displayList();
		
		Node nodeA = new Node();
		nodeA.data= 4;
		Node nodeB = new Node();
		nodeB.data = 3;
		Node nodeC = new Node();
		nodeC.data = 7;
		Node nodeD = new Node();
		nodeD.data = 8;
		
		nodeA.next = nodeB;
		nodeB.next = nodeC;
		nodeC.next = nodeD;
		
		System.out.println(listLength(nodeA));
		System.out.println(listLength(nodeB));
	}
	
	public static int listLength(Node aNode) {
		int length = 0;
		while (aNode != null) {
			length++;
			aNode = aNode.next;
		}
		return length;
	}

}
