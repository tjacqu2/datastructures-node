package ds.doublylinkedlist;



public class NodeDll {
	public int data;
	public NodeDll next;
	public NodeDll previous;
	
	public void displayNode() {
		System.out.println("{ "+ data + "} ");
	}

}
