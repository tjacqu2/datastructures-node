package ds.doublylinkedlist;

public class DoublyLinkedList {
	private NodeDll first;
	private NodeDll last;
	
	public DoublyLinkedList() {
		this.first = null;
		this.last = null;
	}
	
	public boolean isEmpty() {
		return (first == null);
	}
	
	public void insertFirst(int data) {
		NodeDll nodeNew = new NodeDll();
		nodeNew.data = data;
		
		if(isEmpty()) {
			last = nodeNew;
		} else {
			first.previous = nodeNew;
		}
		nodeNew.next = first;
		this.first = nodeNew;
	}
	
	public void insertLast(int data) {
		NodeDll nodeNew = new NodeDll();
		nodeNew.data = data;
		if(isEmpty()) {
			first = nodeNew;
		} else {
			last.next = nodeNew;
			nodeNew.previous = last;			
		}
		last = nodeNew;
	}
	
	//assume non-empty list
	public NodeDll deleteFirst() {
		NodeDll temp = first;
		
		if (first.next == null) {
			last = null;
		} else {
			first.next.previous = null;
		}
		first.next = first;
		return temp;
	}
	//assume non-empty list
	public NodeDll deleteLast() {
		NodeDll temp = last;
		if(first.next == null)	{
			first = null;
		} else {
			last.previous.next = null;
		}
		last = last.previous;
		return temp;
	}
	//assume non-empty list
	public boolean insertAfter(int key, int data) {
		NodeDll current = first;
		while(current.data != key) {
			current = current.next;
			if (current == null) {
				return false;
			}
		}
		NodeDll nodeNew = new NodeDll();
		nodeNew.data = data;
			if(current == last){
				last.next = nodeNew;		//current.next = null;?? doesn't seem right
				nodeNew.previous = last;	//last = newNode;?? doesn't seem right
				last = nodeNew;
			} else {			
			nodeNew.next = current.next;
			current.next.previous = nodeNew;
			}
		nodeNew.previous = current;
		current.next = nodeNew;
		return true;
	}
	//assume non-empty list
	public NodeDll deleteKey(int key) {
		NodeDll current = first;
		while (current.data != key) {
			current = current.next;
			if (current == null) {
				return null;
			}
		}
		if (current == first) {
			first = current.next;
		} else {
			current.previous.next = current.next;
		}
		
		if (current == last) {
			last = current.previous;
		} else {
			current.next.previous = current.previous;
		}
		return current;
	}
	
	public void displayForward() {
		
	}
	
	public void dislayBackward() {
	
	}
}
